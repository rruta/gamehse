(function () {

    'use strict';

    var MainController = function (
        $scope,
        App,
        Menu,
        MenuButton,
        Page,
        Dispatcher,
        Exercise,
        Overlay,
        Subtitles,
        Helper,
        Video,
        Glossary,
        Translation,
        $compile,
        $timeout,
        $rootScope,
        Tracking,
        $location, $anchorScroll,
        GameService
    ) {

        // inizializzo tutte le variabili quando
        // i file di configurazioni sono stati caricati
        App.promise.then(function () {

            $scope.App = App;
            $scope.Page = Page;
            $scope.menu = Menu;
            $scope.MenuButton = MenuButton;
            $scope.module = App.module();
            $scope.Exercise = Exercise;
            $scope.Overlay = Overlay;
            $scope.Subtitles = Subtitles;
            $scope.Helper = Helper;
            $scope.Glossary = Glossary;
            $scope.Translation = Translation;
            $scope.Video = Video;

            // get current video
            $scope.video = function () {

                if (!$scope.page())
                    return false;

                return $scope.page().video();
            };

            // shortcut to get current page
            $scope.page = function () {
                if (!$scope.lesson())
                    return false;

                return $scope.lesson().page();
            };

            $scope.playAudioIfExists = function(){
                if ($scope.page() && $scope.page().html() && $scope.page().html().getAttr('playGameAudio'))
                    GameService.playGameAudio();
            }

            // shortcut to get current lesson
            $scope.lesson = function () {
                if (!$scope.module())
                    return false;

                return $scope.module().lesson();
            };

            // shortcut to get current module
            $scope.module = function () {
                if (!App.module())
                    return false;
                return App.module();
            };


            Video.events.on('fullscreen', function () {
                var videocontrols = $('#video-controls').clone(true, true);
                videocontrols.attr('id', 'video-controls-update');
                videocontrols.removeAttr('ng-if');
                $compile(videocontrols)($scope);
                $('.video-js').append(videocontrols);
                $timeout(function () { $scope.$apply(); }, 1000);
            });

            Video.events.on('fullscreenOut', function () {
                $('#video-controls-update').remove();
                $timeout(function () { $(window).scrollTop() }, 1000);
            });

            App.events.on('module.close', function () {
                // console.log("Are all modules viewed?", App.areAllModulesViewed());

                Tracking.update();
            });

            App.events.on('module.closeScroll', function (lastModuleViewed) {
                if (lastModuleViewed == 0) {
                    $location.hash('section2');
                } else if (lastModuleViewed == 1) {
                    $location.hash('section2');
                } else if (lastModuleViewed == 2) {
                    $location.hash('section3');
                } else if (lastModuleViewed == 3) {
                    $location.hash('section3');
                } else if (lastModuleViewed == 4) {
                    $location.hash('section4');
                } else if (lastModuleViewed == 5) {
                    $location.hash('section4');
                } else if (lastModuleViewed == 6) {
                    $location.hash('section5');
                } else if (lastModuleViewed == 7) {
                    $location.hash('section5');
                } else if (lastModuleViewed == 8) {
                    $location.hash('section6');
                } else if (lastModuleViewed == 9) {
                    $location.hash('section6');
                } else if (lastModuleViewed == 10) {
                    $location.hash('section7');
                } else if (lastModuleViewed == 11) {
                    $location.hash('section7');
                } else if (lastModuleViewed == 12) {
                    $location.hash('section8');
                } else if (lastModuleViewed == 13) {
                    $location.hash('section8');
                } else if (lastModuleViewed == 14) {
                    $location.hash('section8');
                }



                //console.log("qui")

                $anchorScroll();
            });

            $scope.$watch(function () {
                return (screen.availHeight || screen.height - 30) <= window.innerHeight;
            },
                function (value) {
                    if (!value) {
                        $('#video-controls-update').remove();
                    }
                });

            $scope.videoControlsVisible = false;
            $scope.amIOverControls = false;

            $scope.iAmOverControls = function () {
                // console.log("mouseenter");
                $scope.amIOverControls = true;
            }
            $scope.iAmNotOverControls = function () {
                // console.log("mouseleave");
                $scope.amIOverControls = false;
                $scope.videoControlsVisible = false;
            }
            $scope.showVideoControls = function () {
                if ($scope.video()) $scope.video().pause();
                // console.log("showvc");
                $scope.videoControlsVisible = true;//!$scope.videoControlsVisible;
            }

            $scope.areVideoControlsVisible = function () {
                return $scope.videoControlsVisible;
            }

            // var vcv = $timeout(function() { $scope.videoControlsVisible = false; }, 3000);

            // $scope.overVideoControls = function() {
            //     $timeout.cancel(vcv);
            // }

            Video.events.on('ended', function () {
                // console.log("video ended: show controls");
                $scope.videoControlsVisible = true;

                $timeout(function () {
                    $scope.showBtnPopover();
                    // $timeout(function() { $scope.hideBtnPopover(); }, 2000);
                }, 200);
                //App.openMenu(true);

                // If it's the end of the module and
                // there are no exercises or timings,
                // close the module and go back to 
                // the modules index
                if (
                    (App.isAtLastPage() || !App.hasNextPage()) &&
                    (!$scope.page().hasExercise() && !$scope.video().hasObligatoryTiming())
                ) {
                    //App.closeModule();
                }
                $rootScope.$apply()
            });


            $scope.menuExpanded = false;
            $scope.isMenuExpanded = function () {
                return $scope.menuExpanded;
            };

            $scope.toggleExpandedMenu = function () {
                $scope.menuExpanded = !$scope.menuExpanded;
            };

            $scope.loaded = false;
            $scope.hasLoaded = function () {
                return $scope.loaded;
            }

            function load() {
                // console.log('loading...');
                var to = $timeout(function () {
                    // if (!$scope.amIOverControls) {
                    $scope.loaded = true;
                    // console.log('loaded...', $scope.loaded);
                    // }
                }, 1500);
            }

            var __btnPopover = false;

            $scope.showBtnPopover = function () {
                __btnPopover = true;
            };

            $scope.hideBtnPopover = function () {
                __btnPopover = false;
            };

            $scope.isBtnPopoverVisible = function() {
                return __btnPopover;
            };

            // load();

            App.events.on('module.open', function () {
                $scope.loaded = false;
                // console.log("module open: show controls");
                load();
            });

            $scope.isOS = function () {
                return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
            }

        });

        function resizeMap() {
            //alert("ci sono");
            var w = window.innerWidth;
            var h = window.innerHeight;
            // console.log("Window: ", w, 'x', h);

            var mW = $('#main #content .map').width();
            var mH = $('#main #content .map').height();
            // console.log("Map: ", mW, 'x', mH);

            if (mH > h) {
                var newW = h * mW / mH;
                // console.log("Map is HIGH > ", newW);
                $('#main #content .map').css('width', newW);
                $('#main #content .map').css('height', h);
                $('#main #content .map').css({
                    'width': newW,
                    'height': h
                });

            } else {
                $('#main #content .map').css({
                    'width': '',
                    'height': ''
                });
            }
        }

        $timeout(function () {
            $(".map img").one("load", function () {
                // do stuff
                // console.log("image is loaded");
                resizeMap();

            }).each(function () {
                if (this.complete) $(this).load();
            });
        }, 1000);

        $(window).resize(function () {
            resizeMap();
        });

        // console.log( "isMobile.phone", isMobile, isMobile.phone);
    }

    MainController.$inject = [
        '$scope',
        'App',
        'Menu',
        'MenuButton',
        'Page',
        'Dispatcher',
        'Exercise',
        'Overlay',
        'Subtitles',
        'Helper',
        'Video',
        'Glossary',
        'Translation',
        '$compile',
        '$timeout',
        '$rootScope',
        'Tracking',
        "$location", "$anchorScroll",
        "GameService"
    ];

    angular.module('Application')
        .controller('MainController', MainController);

})();