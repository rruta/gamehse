(function () {

    'use strict';

    var SelezioneIndiziController = function ($scope,GameService,Page,App) {
        var model = $scope.page().html() || $scope.page().video().currentTiming().getModule('Html')[0];
        $scope.gameStarted = false;
        $scope.timeOver = false;
        $scope.gameOver = false;
        $scope.timerState = {
            timeStarted: false,
            popupOpened: false
        }

        $scope.state = {
            "INSTRUCTIONS": "INSTRUCTIONS",
            "STARTED" : "STARTED",
            "TIME_OVER": "TIME_OVER",
            "FEEDBACK_VISIBLE": "FEEDBACK_VISIBLE",
            "FEEDBACK_EXPLAINED_VISIBLE": "FEEDBACK_EXPLAINED_VISIBLE",
            "END_GAME_STICKER": "END_GAME_STICKER"
        }
        $scope.currentState = $scope.state.INSTRUCTIONS;

        $scope.currentPolaroidVisibleIndex = 0;

        $scope.viewedPolaroid = {};

        $scope.onNextPolaroidClicked = function(){
            $scope.currentPolaroidVisibleIndex = ($scope.currentPolaroidVisibleIndex+1)%$scope.indiziFuoriPosto.length;
            $scope.viewedPolaroid[$scope.currentPolaroidVisibleIndex] = true;
            if(_.keys($scope.viewedPolaroid).length >= $scope.indiziFuoriPosto.length){
                $scope.finishButtonVisible = true;
            }
        }
        $scope.onPrevPolaroidClicked = function(){
            $scope.currentPolaroidVisibleIndex-=1;
            if ($scope.currentPolaroidVisibleIndex<0){
                $scope.currentPolaroidVisibleIndex = $scope.indiziFuoriPosto.length-1;
            }
        }

        var listaIndizi = model.getAttr('listaIndizi');
        var tmpObj = listaIndizi[Math.floor(Math.random() * (listaIndizi.length))];
        if (App.config().getAttr('isProofingMode')){
            var pos = prompt("quale scenario vuoi testare? Inserisci il numero [0,4]");
            tmpObj = listaIndizi[pos];
        }

        $scope.indizi = tmpObj.indizi;
        $scope.backgroundImg = tmpObj.backgroundImg;

        var SELECTABLE_COUNT = 5;
        var currentIndiziCount = 1;
        var RIGHT_CHOICE_AUDIO = new buzz.sound("audio/sfx/right_sound",{ formats: [ "mp3"], preload: true});


        var FEEDBACK_AUDIO = new buzz.sound("audio/sfx/EpicDramaticCinematicTrailer",{ formats: [ "mp3"], preload: true, volume: 20});



        $scope.nextPage = function(){
            Page.next();
        }

        $scope.showFeedbackExplain = function(){
            $scope.viewedPolaroid[$scope.currentPolaroidVisibleIndex] = true;
            $scope.currentState = $scope.state.FEEDBACK_EXPLAINED_VISIBLE;
            GameService.stopGameAudio();
            //GAME_AUDIO.stop();
            //FEEDBACK_AUDIO.play();
        }

        $scope.closeTimeOverPopup = function(){
            //TweenMax.to(".istruzioni", 0.3, {scale: 0.5, alpha: 0, clearProps:"all", onComplete: ()=>{
                $scope.nextIndizio();
                $scope.$apply();
            //}});
        }

        $scope.openTimeOverPopup = function(){
            $scope.currentState = $scope.state.TIME_OVER;
            GameService.addTimeoutTimer(15000, "GAME1");
        }

        $scope.finishGame = function(){
            GameService.stopGameAudio();
            //GAME_AUDIO.stop();
            $scope.currentState = $scope.state.END_GAME_STICKER;
            $scope.page().viewed();
        }

        $scope.nextIndizio = function(){
            if (currentIndiziCount < SELECTABLE_COUNT) {
                currentIndiziCount++;
                $scope.timerState.timeStarted = true;
                $scope.currentState = $scope.state.STARTED;
            }else{
                FEEDBACK_AUDIO.play();
                $scope.totalScore = 0;
                $scope.timerState.timeStarted = false;
                $scope.progressGame = _.filter(GameService.getGame1Progress(), function(indizio){
                    return indizio.points > 0;
                });
                $scope.wrongAnswers = _.filter(GameService.getGame1Progress(), function(indizio){
                    return (indizio.points < 1);
                });
                $scope.indiziFuoriPosto = _.filter($scope.indizi, function(indizio){
                    return indizio.isCorrect;
                });
                $scope.timeOverAnswers = _.range(5 - $scope.progressGame.length - $scope.wrongAnswers.length);
                //if (($scope.progressGame.length + $scope.wrongAnswers.length) > 0){
                    $scope.currentState = $scope.state.FEEDBACK_VISIBLE;
                    GameService.stopGameAudio();
                    //GAME_AUDIO.stop();

                $scope.totalScore = _.reduce($scope.progressGame, function(a, b){
                    return a + b.points;
                }, 0)
                    //$scope.totalScore = GameService.calcTotalScoreById('GAME1');
                    GameService.saveScore("GAME1");
                /*}else{
                    $scope.currentState = $scope.state.END_GAME_STICKER;
                }*/
            }
        }

        $scope.indizioClicked = function(indizio){
            if (($scope.currentState == $scope.state.STARTED) && !$scope.gameOver && !indizio.selected) {
                RIGHT_CHOICE_AUDIO.stop();
                RIGHT_CHOICE_AUDIO.play();
                indizio.selected = true;
                GameService.addScoreGame1(indizio.id, $scope.timeElapsedMs, indizio.isCorrect, indizio.img);
                $scope.nextIndizio();
                $scope.$broadcast("RESTART_TIMER");
            }
        }

        $scope.startGame = function(){
            $scope.currentState = $scope.state.STARTED;
            
            //direttiva timer
            $scope.timerState.timeStarted = true;
            //GAME_AUDIO.play();
        }

        $scope.$on('$destroy', function() {
            //GAME_AUDIO.stop();
            GameService.stopGameAudio();
            FEEDBACK_AUDIO.stop();
        });

        //initAudioForMobileSupport();
        //GAME_AUDIO.play();

    };

    SelezioneIndiziController.$inject = ['$scope','GameService','Page','App'];

    angular.module('Application')
        .controller('SelezioneIndiziController', SelezioneIndiziController);
})();