(function () {

    'use strict';

    var FrasiIndiziatoController = function ($scope,GameService,Page,$timeout) {
        var model = $scope.page().html() || $scope.page().video().currentTiming().getModule('Html')[0];
        $scope.gameStarted = false;

        $scope.timerState = {
            timeStarted: false,
            popupOpened: false
        }

        $scope.state = {
            "INSTRUCTIONS": "INSTRUCTIONS",
            "STARTED" : "STARTED",
            "TIME_OVER": "TIME_OVER",
            "FEEDBACK_VISIBLE": "FEEDBACK_VISIBLE",
            "CONFIRM_POPUP_VISIBLE": "CONFIRM_POPUP_VISIBLE",
            "CONFIRM_POPUP_HIDDEN": "CONFIRM_POPUP_HIDDEN"
        }
        $scope.currentState = $scope.state.INSTRUCTIONS;

        var listaFrasi = model.getAttr('listaFrasi');
        var currentIndiziIndex = 0;
        //var tmpObj = listaFrasi[Math.floor(Math.random() * (listaFrasi.length))];


        $scope.initIndiziati = function(){
            $scope.indiziati = listaFrasi[currentIndiziIndex].indiziati;//tmpObj.indiziati;
            $scope.title = listaFrasi[currentIndiziIndex].title;
                $scope.backgroundImg = listaFrasi[currentIndiziIndex].backgroundImg;
            $scope.selectedIndiziato = null;
        }

        $scope.initIndiziati();

        const SELECTABLE_COUNT = listaFrasi.length;
        //var currentIndiziCount = 1;
        const RIGHT_CHOICE_AUDIO = new buzz.sound("audio/sfx/right_sound",{ formats: [ "mp3"], preload: true});

        //const GAME_AUDIO = new buzz.sound("audio/sfx/TheMysteryLoop",{ formats: [ "mp3"], preload: true, loop: true});


        const FEEDBACK_AUDIO = new buzz.sound("audio/sfx/EpicDramaticCinematicTrailer",{ formats: [ "mp3"], preload: true, volume: 20});

        // workaround to play audio on ios devices without user touch event
        var initAudioForMobileSupport = function(){
            FEEDBACK_AUDIO.bindOnce("play", function(){
                FEEDBACK_AUDIO.pause();
                FEEDBACK_AUDIO.unmute();
            });
            FEEDBACK_AUDIO.mute();
            FEEDBACK_AUDIO.play();
        }

        $scope.showFeedback = function() {
            $scope.progressGame3 = GameService.getGame3Progress();
            $scope.progressGame = _.filter($scope.progressGame3, function(indizio){
                return indizio.points > 0;
            });

            $scope.correctAnswers = listaFrasi.map(function(testimonianzaObj){
                return testimonianzaObj.indiziati.filter(function(indiziato){
                    return indiziato.isCorrect;
                })
            }).map(function(indiziatoObj){
                return indiziatoObj[0].testimonianza;
            });

            $scope.currentState = $scope.state.FEEDBACK_VISIBLE;
        }

        $scope.nextPage = function(){
            $scope.page().viewed();
            Page.next();
        }

        $scope.closeTimeOverPopup = function(){
            $scope.currentState = $scope.state.STARTED;
            $scope.onAnswerGiven(true);
        }

        $scope.openTimeOverPopup = function(){
            $scope.currentState = $scope.state.TIME_OVER;
            $scope.timerState.timeStarted = false;
            GameService.addTimeoutTimer(30000, "GAME3");
        }


        $scope.indiziatoClicked = function(indiziato){
            if (!indiziato.selected){
                indiziato.selected = true;
                $scope.currentState = $scope.state.CONFIRM_POPUP_VISIBLE;
                $scope.selectedIndiziato = indiziato;
            }
        }

        $scope.addScore = function(){
            GameService.addScoreGame3($scope.selectedIndiziato.id, $scope.timeElapsedMs, $scope.selectedIndiziato.isCorrect, $scope.selectedIndiziato.img);
                $scope.selectedIndiziato.confirmed = true;
                $scope.currentState = $scope.state.CONFIRM_POPUP_HIDDEN;
                $scope.onAnswerGiven();
        }

        $scope.cancel = function(){
            $scope.selectedIndiziato.selected = false;
            $scope.selectedIndiziato = null;
            $scope.currentState = $scope.state.STARTED;
        }

        $scope.onAnswerGiven = function(shouldSkipTimer){
            RIGHT_CHOICE_AUDIO.stop();
            RIGHT_CHOICE_AUDIO.play();
            $scope.timerState.timeStarted = false;

            $timeout(function(){
                if (currentIndiziIndex<SELECTABLE_COUNT-1){
                    $timeout(function(){
                        currentIndiziIndex++;
                        $scope.initIndiziati();
                        $scope.currentState = $scope.state.STARTED;
                        //$scope.$broadcast("RESTART_TIMER");
                        $scope.timerState.timeStarted = true;
                    },50);
                }else{
                    $scope.showFeedback();
                    GameService.stopGameAudio();
                    FEEDBACK_AUDIO.play();
                    GameService.saveScore("GAME3");
                }
            }, shouldSkipTimer ? 0 : 2500) // se per l'ultimo indizio scade il tempo non aspettare i 2 secondi



            //$scope.$apply();

        }

        $scope.$on('$destroy', function() {
            GameService.stopGameAudio();
            FEEDBACK_AUDIO.stop();
        });

        $scope.startGame = function(){
            initAudioForMobileSupport();
            $scope.currentState = $scope.state.STARTED;
            $scope.timerState.timeStarted = true;
        }
    };

    FrasiIndiziatoController.$inject = ['$scope','GameService','Page','$timeout'];

    angular.module('Application')
        .controller('FrasiIndiziatoController', FrasiIndiziatoController);
})();