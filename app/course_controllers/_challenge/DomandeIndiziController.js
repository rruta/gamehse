(function () {

    'use strict';

    var DomandeIndiziController = function ($scope,GameService,Page,$timeout,App) {
        var model = $scope.page().html() || $scope.page().video().currentTiming().getModule('Html')[0];
        $scope.timeOver = false;
        $scope.gameOver = false;
        //$scope.popupAnswerVisible = false;
        $scope.timerState = {
            timeStarted: false,
            popupOpened: false
        }

        $scope.state = {
            "INSTRUCTIONS": "INSTRUCTIONS",
            "STARTED" : "STARTED",
            "TIME_OVER": "TIME_OVER",
            "FEEDBACK_VISIBLE": "FEEDBACK_VISIBLE",
            "QUESTION_POPUP_VISIBLE": "QUESTION_POPUP_VISIBLE",
            "END_GAME_STICKER": "END_GAME_STICKER"
        }
        $scope.currentState = $scope.state.INSTRUCTIONS;
        var currentScrivaniaCount = 1;
        var currentIndiziCount = 1;

        if (App.config().getAttr('isProofingMode')){
            currentScrivaniaCount = 2;
        }

        var listaIndizi = model.getAttr('listaIndizi');
        //var tmpObj = listaIndizi[Math.floor(Math.random() * (listaIndizi.length))];


        // workaround to play audio on ios devices without user touch event
        var initAudioForMobileSupport = function(){

            FEEDBACK_AUDIO.bindOnce("play", function(){
                FEEDBACK_AUDIO.pause();
                FEEDBACK_AUDIO.unmute();
            });
            FEEDBACK_AUDIO.mute();
            FEEDBACK_AUDIO.play();
        }

        var initIndizi = function(){
            var tmpObj = listaIndizi[currentScrivaniaCount-1];
            $scope.indizi = tmpObj.indizi;
            $scope.backgroundImg = tmpObj.backgroundImg;
            currentIndiziCount = 1;
        }
        initIndizi();

        const SELECTABLE_COUNT = 5;

        //const RIGHT_CHOICE_AUDIO = new buzz.sound("audio/sfx/right_sound",{ formats: [ "mp3"], preload: true});
        //const GAME_AUDIO = new buzz.sound("audio/sfx/TheMysteryLoop",{ formats: [ "mp3"], preload: true, loop: true});

        const FEEDBACK_AUDIO = new buzz.sound("audio/sfx/EpicDramaticCinematicTrailer",{ formats: [ "mp3"], preload: true, volume: 20});

        $scope.feedbackScrivaniaCount = 0;

        $scope.nextPage = function(){
            Page.next();
        }

        $scope.showFeedback = function(_idScrivania) {
            $scope.feedbackImg = listaIndizi[_idScrivania].feedbackImg;
            var progressGame2 = GameService.getGame2Progress();
            //console.log("progressGame2",progressGame2)
            /*if (App.config().getAttr('isProofingMode')){
                progressGame2 = JSON.parse('[{"id":1,"elapsedTime":271,"points":300,"img":"forbici.png","idScrivania":0,"$$hashKey":"object:198"},{"id":2,"elapsedTime":220,"points":0,"img":"sgrassatore.png","idScrivania":0,"$$hashKey":"object:204"},{"id":3,"elapsedTime":61,"points":300,"img":"candela.png","idScrivania":0,"$$hashKey":"object:199"},{"id":4,"elapsedTime":120,"points":300,"img":"cassetto_aperto.png","idScrivania":0,"$$hashKey":"object:200"},{"id":0,"elapsedTime":180,"points":300,"img":"zaino.png","idScrivania":1},{"id":1,"elapsedTime":150,"points":0,"img":"pc_acceso.png","idScrivania":1},{"id":2,"elapsedTime":331,"points":300,"img":"ombrello.png","idScrivania":1},{"id":3,"elapsedTime":452,"points":0,"img":"lampada_accesa.png","idScrivania":1},{"id":0,"elapsedTime":360,"points":0,"img":"poggiapiedi.png","idScrivania":2},{"id":1,"elapsedTime":220,"points":300,"img":"lampada_spenta.png","idScrivania":2},{"id":2,"elapsedTime":262,"points":0,"img":"telefono.png","idScrivania":2},{"id":3,"elapsedTime":450,"points":0,"img":"sedia.png","idScrivania":2},{"id":4,"elapsedTime":241,"points":0,"img":"mouse.png","idScrivania":2}]');
            }*/

            $scope.progressGame = _.filter(progressGame2, function(indizio){
                return (indizio.idScrivania === _idScrivania) && indizio.points > 0;
            });
            $scope.wrongAnswers = _.filter(progressGame2, function(indizio){
                return (indizio.idScrivania === _idScrivania) && (indizio.points < 1);
            });
            $scope.indiziFuoriPosto = _.filter($scope.indizi, function(indizio){
                return (indizio.idScrivania === _idScrivania) && indizio.isCorrect;
            });
            $scope.timeOverAnswers = _.range(5 - $scope.progressGame.length - $scope.wrongAnswers.length);
            $scope.totalScore = _.reduce($scope.progressGame, function(a, b){
                return a + b.points;
            }, 0)

            $scope.currentState = $scope.state.FEEDBACK_VISIBLE;
        }

        $scope.showEndGameSticker = function() {
            if ($scope.feedbackScrivaniaCount < 2){
                $scope.showFeedback(++$scope.feedbackScrivaniaCount);
            }else{
                $scope.currentState = $scope.state.END_GAME_STICKER;
            }
            $scope.page().viewed();
        }

        $scope.closeTimeOverPopup = function(){
            $scope.currentState = $scope.state.STARTED;
            $scope.currentIndizio = null;
            $scope.onAnswerGiven();
        }

        $scope.openTimeOverPopup = function(){
            //$scope.onAnswerGiven();
            $scope.currentState = $scope.state.TIME_OVER;
            GameService.addTimeoutTimer(15000, "GAME2", currentScrivaniaCount-1, $scope.currentIndizio.img);

        }

        /*var hideQuestionPopup = function(){
            gsap.to(".question-popup", {autoAlpha: 0, duration: 0, clearProps: 'all'});
        }*/

        $scope.indizioClicked = function(_indizio){

            if (!_indizio.selected){
                //RIGHT_CHOICE_AUDIO.stop();
                //RIGHT_CHOICE_AUDIO.play();
                //direttiva timer
                //$scope.timerState.timeStarted = true;



                $timeout(function(){
                    gsap.to(".question-popup", {autoAlpha: 1, duration: 0.5, onComplete: ()=>{
                            $scope.timerState.timeStarted = true;
                            $scope.$apply();
                        }
                    });
                },0)
                $scope.currentIndizio = _indizio;
            }
        }

        $scope.addScore = function(event, answer){
            event.stopPropagation();
            $scope.currentIndizio.givenAnswer = answer;
            GameService.addScoreGame2($scope.currentIndizio.id, $scope.timeElapsedMs, answer===$scope.currentIndizio.isCorrect, $scope.currentIndizio.img, currentScrivaniaCount-1);
            $scope.tl.timeScale(3).reverse();

            $scope.currentIndizio.selected = true;
            $scope.timerState.timeStarted = false;

            //hideQuestionPopup();
            $scope.currentState = $scope.state.STARTED;
        }

        $scope.onAnswerGiven = function(){
            //$timeout(function(){
                if (currentIndiziCount<SELECTABLE_COUNT){
                    currentIndiziCount++;
                    $scope.currentIndizio = null;
                    $scope.selectedAnswer = null;
                    $scope.setNextIndizio();
                }else{
                    if (currentScrivaniaCount<3){
                        currentScrivaniaCount++;
                        $scope.currentIndizio = null;
                        $scope.selectedAnswer = null;
                        initIndizi();
                        $scope.setNextIndizio();
                    }else{
                        GameService.stopGameAudio();
                        FEEDBACK_AUDIO.play();

                        $scope.showFeedback($scope.feedbackScrivaniaCount);
                        GameService.saveScore("GAME2");
                    }
                }/*


                $scope.$apply();
            },0);*/

        }

        $scope.startGame = function(){
            initAudioForMobileSupport();
            $scope.currentState = $scope.state.STARTED;
            $scope.setNextIndizio();
        }

        var MAX_WIDTH_ZOOMED_INDIZIO_PERC = 24;
        $scope.setNextIndizio = function(){
            $scope.currentState = $scope.state.QUESTION_POPUP_VISIBLE;
            $scope.currentIndizio = $scope.indizi[currentIndiziCount-1];



            var questionPopupWidth = $("#domande-indizi").innerWidth();
            $timeout(function(){
                $scope.tl = gsap.timeline();

                var indizioWidth = $(".indizio.current").innerWidth();
                var indizioHeight = $(".indizio.current").innerHeight();

                var maxHeight = $("#domande-indizi").innerHeight()*80/100;


                var newHeight = indizioHeight * (questionPopupWidth*MAX_WIDTH_ZOOMED_INDIZIO_PERC/100) / indizioWidth;

                const OPACITY_ANIM_DURATION = 0.6;
                var tlYoYoIndizio = gsap.timeline();
                tlYoYoIndizio.to(".indizio.current", {scaleX: 1.2, scaleY: 1.2, duration: 0.4, yoyo: true, repeat: 1, ease: "power1.inOut", onComplete: ()=>{

                        var tlBackdropAndZoom = gsap.timeline();
                        tlBackdropAndZoom.to(".backdrop", {autoAlpha: 1,duration: OPACITY_ANIM_DURATION}, "+=0")
                        tlBackdropAndZoom.to(".indizio.current", 0.4, {height: Math.min(newHeight, maxHeight) +"px", left: "3%", top: "50%", xPercent: "0", transform: 'translate(0, -'+Math.min(newHeight, maxHeight)/2+'px)',/*translateY: "-50%",/*-newHeight+'px',*/ ease: "power2.inOut", onComplete: ()=>{
                                $scope.indizioClicked($scope.indizi[currentIndiziCount-1]);
                                $scope.$apply();
                            },
                            onReverseComplete: () => { $timeout(function(){$scope.onAnswerGiven()},1000) }
                        }, "-="+OPACITY_ANIM_DURATION);
                        $scope.tl = tlBackdropAndZoom;
                    }
                });
            },800)
        }


        $scope.$on('$destroy', function() {
            GameService.stopGameAudio();
            FEEDBACK_AUDIO.stop();
        });
    };

    DomandeIndiziController.$inject = ['$scope','GameService','Page','$timeout','App'];

    angular.module('Application')
        .controller('DomandeIndiziController', DomandeIndiziController);
})();