(function () {

    'use strict';

    var SelezionePasswordController = function ($scope,GameService,Page,$rootScope,$timeout) {
        var model = $scope.page().html() || $scope.page().video().currentTiming().getModule('Html')[0];
        $scope.alertMessageVisible = false;
        $scope.singleChoiceVisible = false;
        $scope.state = {
            selectedPassword : null
        }
        $scope.isTrackingSent = false;

        $scope.passwordList = [
            "rete2015", "telecamera9988", "responsabile1111"
        ]
        const CORRECT_PASSWORD_INDEX = 0;
        var numeroTentativi = 0;

        $scope.isCorrect = function(){
            if ($scope.passwordList[CORRECT_PASSWORD_INDEX] === $scope.state.selectedPassword){
                if (!$scope.isTrackingSent){
                    $scope.isTrackingSent = true;
                    $scope.page().viewed();
                }
                return true;
            }
            return false;
        }

        $scope.onConfirm = function(value){
            if (value){
                $scope.state.selectedPassword = value;
            }
            if ($scope.isCorrect()){
                $scope.confirmButtonVisible = true;
            }else{
                $scope.confirmButtonVisible = false;
                $scope.alertMessageVisible = true;
                numeroTentativi++;


                if (!$scope.singleChoiceVisible){
                    $scope.state.selectedPassword = null;
                }

                if(numeroTentativi >= 2 && $scope){
                    $scope.singleChoiceVisible = true;
                }
            }
        }

        $scope.nextPage = function(){
            Page.next();
        }
    };

    SelezionePasswordController.$inject = ['$scope','GameService','Page','$rootScope','$timeout'];

    angular.module('Application')
        .controller('SelezionePasswordController', SelezionePasswordController);
})();