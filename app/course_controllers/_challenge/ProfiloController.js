(function () {

    'use strict';

    var ProfiloController = function ($scope,GameService) {
        var score = GameService.calcTotalScore();
        $scope.score = score;
        console.log("calculated score: ", score);
        $scope.feedbackText = "";
        if (score <= 1500){
            $scope.feedbackText = "Conoscenza della sicurezza da migliorare e attenzione ai pericoli da sviluppare";
        }
        if (1500 < score && score <= 2300){
            $scope.feedbackText = "Conoscenza della sicurezza da affinare e attenzione ai pericoli da migliorare";
        }
        if (2300 < score && score <= 4600){
            $scope.feedbackText = "Buona conoscenza della sicurezza ma spirito d'osservazione da migliorare. Lavorare anche sulla velocità nell’individuare i pericoli";
        }
        if (4600 < score && score <= 4900){
            $scope.feedbackText = "Ottima conoscenza della sicurezza e scrupolosa attenzione verso i pericoli. Velocità nell’individuare i pericoli migliorabile";
        }
        if (4900 < score && score <= 6000){
            $scope.feedbackText = "Ottima conoscenza della sicurezza e scrupolosa attenzione ai pericoli. Capacità di individuare velocemente i pericoli ottima ma non ancora perfetta";
        }
        if (6000 < score && score <= 6900){
            $scope.feedbackText = "Massima conoscenza della sicurezza e scrupolosa attenzione ai pericoli. Massima reattività!";
        }

    };

    ProfiloController.$inject = ['$scope','GameService'];

    angular.module('Application')
        .controller('ProfiloController', ProfiloController);
})();