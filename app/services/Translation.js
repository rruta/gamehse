(function()
{
  'use strict';

  var TranslationService = function(Model, Event, $http, $translate, $timeout)
  {
    var all = [];

    var promise = false;

    var eventer = Event.create('translation');

    var TranslationModel = function(attrs)
    {
        var model = Model.create({
            attrs: attrs,
            name: 'translation'
        });

        model.isDisabled = function() {
          return model.getAttr('disabled');
        };

        model.active = function() {
          eventer.emit('change');
          if(!model.isDisabled())
          {
              $translate.use(model.getAttr('lang'));
          }

          $timeout(function() {
            $(window).trigger('resize');
              
          },1000)
          //$(window).trigger('resize');
        };

        model.isActive = function(){
            return $translate.use() == model.getAttr('lang');
        };

        return model;
    };

    var service =
    {
      all: function()
      {
        return all;
      },
      listLang: function()
      {
        var array = [];
        _.each(all, function(item)
        {
          array.push(item.getAttr('lang'));
        });

        return array;
      },
      promise: promise,
      init: function()
      {
        promise = $http.get('config/translations.json')
          .then(function(response)
          {
            _.each(response.data.translations, function(item){
              all.push(new TranslationModel(item));
            });
          });
      },
      current: function () {
          var _return = false;
          _.each(all, function(item){
              if(item.isActive()) {
                  _return = item;
                  return false;
              }
          });
          return _return;
      },
      isSelected: function() {
          return this.current() ? this.current() : false;
      },
      events: eventer
    };

    return service;
  };

  TranslationService.$inject = ['Model', 'Event', '$http', '$translate', '$timeout'];

  angular.module('Application')
    .service('Translation', TranslationService)
    .config(['$translateProvider', function($translateProvider)
		{
					$translateProvider
						.useStaticFilesLoader({
							prefix: 'config/locales/locale-',
							suffix: '.json'
					})
					.useSanitizeValueStrategy('sanitizeParameters')
					.useLoaderCache(true)
					.uniformLanguageTag('bcp47')
					.preferredLanguage('it')
          ;
		}])
    .run(['Translation', 'App', '$rootScope', function(Translation, App, $rootScope)
    {
        Translation.init();

        // when app is loaded
        App.promise.then(function()
        {
          // when language change
          $rootScope.$on('$translateChangeEnd', function()
          {
           
            if (App.module()) {
                // necessary to update current video when language change
                App.module().lesson().page().updateVideo();
              }
          });
        });
    }]);

})();
