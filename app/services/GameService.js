app.service('GameService', ['Tracking', function (Tracking) {

    var _progress = {
    }

    var GAME_AUDIO = new buzz.sound("audio/sfx/TheMysteryLoop",{ formats: [ "mp3"], preload: true, loop: true});

    var recoverScore = function(_idGame, _points, _time){
        if (!_progress[_idGame]){
            _progress[_idGame] = [];
        }
        _progress[_idGame].push({points : Number(_points), elapsedTime : (_time*1 || -1)});
    }

    const CORRECT_POINTS = 100;

    var strSuspendData = Tracking.getSuspendData();
    //var strSuspendData = "GAME1:3300:t25461//GAME2:2100:t13193//GAME3:300:t3632||TOTAL:5700";
    //var strSuspendData = "";
    if (strSuspendData && (strSuspendData.length > 0)){
        var arrayScore = strSuspendData.split("||")[0].split("//"); //[ "GAME1:600", "GAME2:2400", "GAME3:1000" ]
        arrayScore.forEach((missionScore)=>{
            var splittedScore = missionScore.split(":");
            recoverScore(splittedScore[0], splittedScore[1], splittedScore[2].replace("t",""));
        })
    }

    var buildStringSuspendData = function(pointsPerMission){
        var keys = Object.keys(pointsPerMission);
        var finalTotalScore = 0;

        var buildedStr = keys.map(function(key){
            var tot = pointsPerMission[key].reduce((a,b)=>{
                return a + b.points;
            },0)
            var elapsedTotalTime = pointsPerMission[key].reduce((a,b)=>{
                return a + b.elapsedTime;
            },0)

            finalTotalScore += tot;
            return key + ":" + tot + ":t" + elapsedTotalTime;
        }).join("//");
        return buildedStr+"||TOTAL:"+finalTotalScore;
    }

    var getMultiplier = function(_idGame, _elapsedTime){
        if (_idGame === "GAME3"){
            if (_elapsedTime < 15000){
                return 3;
            }else if(_elapsedTime < 22000){
                return 2;
            }
        }else{
            if (_elapsedTime < 9000){
                return 3;
            }else if(_elapsedTime < 11000){
                return 2;
            }
        }
        return 1;
    }

    var addScore = function(_id, _elapsedTime, _isCorrect, _img, _idGame, _idScrivania){
        var _points = _isCorrect ? CORRECT_POINTS : 0;
        _points *= getMultiplier(_idGame, _elapsedTime);
        //console.log("DEBUG:: aggiungo punteggio "+ _idGame +" : id[" + _id + "] img[" + _img + "] time["+_elapsedTime+"] points[" + _points + "]");

        var data = {
            id: _id,
            elapsedTime: _elapsedTime,
            points: _points,
            img: _img
        }

        // only for GAME2
        if (_idGame === 'GAME2'){
            data.idScrivania = _idScrivania;
        }

        if (!_progress[_idGame]){
            _progress[_idGame] = [];
        }
            _progress[_idGame].push(data);
    }

    var addTimeoutTimer = function(_time, _idGame, _idScrivania, _img){
        addScore("timeout",_time,false, _img, _idGame, _idScrivania);
        /*var data = {
            id: "timeout",
            elapsedTime: _time,
            points: 0,
            img: null
        }*/
    }

    var saveScore = function(_idGame){

        // aggiunge la voce es. GAME1:0 alla stringa di tracciamento (in caso tutti gli indizi siano andati in timeover
        if (!_progress[_idGame]){
            _progress[_idGame] = [{points:0}]
        }
        var buildedStr = buildStringSuspendData(_progress);
        Tracking.updateSuspendData(buildedStr);
    }

    return {
        addScoreGame1: function(_id, _elapsedTime, _isCorrect, _img){
            addScore(_id, _elapsedTime, _isCorrect, _img, "GAME1");
        },
        addScoreGame2: function(_id, _elapsedTime, _isCorrect, _img, _idScrivania){
            addScore(_id, _elapsedTime, _isCorrect, _img, "GAME2", _idScrivania);
        },
        addScoreGame3: function(_id, _elapsedTime, _isCorrect, _img){
            addScore(_id, _elapsedTime, _isCorrect, _img, "GAME3");
            Tracking.complete();
        },
        getProgress: function(){
            return _progress;
        },
        getGame1Progress: function(){
            return _progress["GAME1"];
        },
        getGame2Progress: function(){
            return _progress["GAME2"];
        },
        getGame3Progress: function(){
            return _progress["GAME3"];
        },
        calcTotalScore: function(){
            var keys = Object.keys(_progress);
            var total = 0;
            keys.forEach((key) => {
                total += _progress[key].reduce((a,b) => {
                    return a + b.points;
                },0);
            })
            return total;
        },
        saveScore: function(_idGame){
            saveScore(_idGame);
        },
        addTimeoutTimer: function(_time, _idGame, _idScrivania, _img){
            addTimeoutTimer(_time, _idGame, _idScrivania, _img);
        },
        stopGameAudio: function(){
            GAME_AUDIO.stop();
        },
        playGameAudio: function(){
            GAME_AUDIO.stop();
            GAME_AUDIO.play();
        }
    };
}]);