app.factory('App', ['$http', 'Module', 'Model', 'Panel', 'Event', '$window', '$timeout', '$rootScope', 'Sound',"$location", "$anchorScroll",
    function ($http, Module, Model, Panel, Event, $window, $timeout, $rootScope, Sound, $location, $anchorScroll) {

        var config = false;
        var loaded = false;
        var loadedModule=false;

        var modules = [];
        var pages = [];
        var _openMenu = false;

        var audioEndedInstance = false;
        var audioEnded = "ended";

        var audioEndedPlayed = false;

        var eventer = Event.create('app');

        var panels = {
            resume: Panel.create('app.resume'),
            closer: Panel.create('app.closer'),
            logout: Panel.create('app.logout')
        };

        // modulo in cui mi trovo adesso
        var currentModule = false;

        // a little object of configurations
        function Config(data) {
            var model = Model.create({
                attrs: data,
                name: 'app.config'
            });

            // check if app navigation is free
            model.isFree = function () {
                return !model.getAttr('restricted');
            };

            /**
             * @description check if navigation is limited in lesson
             * @return {boolean}
             */
            model.isNavLimitedOnLesson = function () {
                return model.getAttr('layout').nav === 'limited';
            };

            return model;
        };

        var promise = $http.get('config/index.json').then(function (response) {
            config = new Config(response.data);
            loaded = true;
            modules = Module.collection(config.getAttr('structure').modules);
        });

        $($window).resize(function () {
            if ($('slick').length) {
                var slick = $('slick')[0].slick;
                slick.slickSetOption('slidesToShow', service.getSlidesToShow(), true);
                slick.slickSetOption('slidesToScroll', 1, true);
                slick.reinit();
            }
        });

        var service = {
            promise: promise,
            loaded: function () {
                return loaded;
            },
            pages: function () {
                if (!pages.length) {
                    _.each(modules.all(), function (m) {
                        _.each(m.lessons().all(), function (l) {
                            pages = pages.concat(l.pages().all());
                        });
                    });
                }
                ;
                return pages;
            },
            course: function () {
                return this.config().getAttr('structure').course;
            },
            /**
             * @description get current page number
             * @return {*}
             */
            getCurrentPageNumber() {
                if (!this.module()) return;

                // if navigation is limited only in lesson
                if (this.config().isNavLimitedOnLesson())
                    return this.module().lesson().pages().getCurrentNumber();

                let page = this.module().lesson().page();
                return _.indexOf(this.pages(), page) + 1;
            },
            /**
             * @description get max page number
             * @return int
             */
            getMaxPageNumber() {
                if (this.config().getAttr('layout').nav === 'limited')
                    return this.module().lesson().pages().count();
                return this.pages().length
            },
            // ottengo la struttura dei moduli
            structure: function () {
                return modules;
            },
            layout: function () {
                return config.layout;
            },
            // ottengo la pagina corrente
            page: function () {
                return this.lesson().page();
            },
            // ottengo la lezione corrente
            lesson: function () {
                return this.module().lesson();
            },
            // ottengo il modulo corrente
            module: function () {
                if (currentModule === false) return;
                return this.structure().current();
            },
            modules: function () {
                return this.structure();
            },

            getLastModuleViewed: function () {
                var lastIndexViewd = -1;
                for (var i = 0; i < modules.all().length; i++) {

                    console.log("modules.all()[i]",modules.all().length)
                    if (!modules.all()[i].isViewed()) {
                        //console.log(modules.all()[i].isViewed())
                        lastIndexViewd = i;
                        break;
                    }
                }
                console.log(lastIndexViewd)
                return lastIndexViewd == -1 ? 0 : lastIndexViewd

                //App.modules().get(0).isViewed()
            },

            nextModule: function () {
                if (this.structure().isAtTheEnd())
                    return false;

                this.modules().next();
                this.module().setCurrentLesson(0);
                return this;
            },
            prevModule: function () {
                if (this.structure().isAtTheStart())
                    return false;

                if (currentModule > 0)
                    currentModule--;

                this.modules().setCurrent(currentModule);
                this.module().setCurrentLesson(0);
                return this;
            },

            openMenu: function (val) {
                return _openMenu = val;
            },

            getOpenMenu: function () {
                // console.log("_openMenu "+ _openMenu)
                return _openMenu;
            },
            /**
             * @description open module
             * @param i
             * @return {*}
             */
            goToModule: function (moduleIndex) {
                /*if(i>0 && !config.isFree() && !this.modules().get(i-1).isViewed())
                    return false;
                    */
                loadedModule=true;
                eventer.emit('module.open.pre');
                currentModule = moduleIndex;
                this.modules().setCurrent(currentModule);
                this.module().setCurrentLesson(0);

                // if(!this.module().lesson().pages().isAtTheStart()) {
                //     panels.resume.open();
                // }
                // else {
                var currentPage = this.getCurrentPageNumber() - 1;
                this.module().lesson().setCurrentPage(currentPage);
                // }
                eventer.emit('module.open');

                return this;
            },
            forcedGoTo: function(iModule, iLesson, iPage) {
                var _this = this;

                _this.modules().setCurrent(iModule);
                _this.module().setCurrentLesson(iLesson);
                _this.module().lesson().setCurrentPage(iPage);

                eventer.emit('menu.change');

                return _this;
            },
            goTo: function (iModule, iLesson, iPage) {
                var _this = this;

                if (
                    !_this.module().lessons().get(iLesson).pages().get(iPage).isViewed()
                    && !config.isFree()
                ) {
                    return;
                }

                _this.modules().setCurrent(iModule);
                _this.module().setCurrentLesson(iLesson);
                _this.module().lesson().setCurrentPage(iPage);

                eventer.emit('menu.change');

                return _this;
            },
            closeModule: function () {
                // if (this.modules().get(0).isViewed() &&
                //     this.modules().get(1).isViewed() &&
                //     this.modules().get(2).isViewed() &&
                //     this.modules().get(3).isViewed() &&
                //     !this.config().getAttr('layout').disableFinalAudio
                // ) {
                //     audioEndedInstance = false;
                //     audioEnded = "ended";
                //     if (audioEnded && !this.hasFinalAudioBeenPlayed()) {
                //         audioEndedInstance = Sound.create({
                //             src: audioEnded 
                //         });
                //         audioEndedInstance.play();
                //         this.setFinalAudioAsPlayed();
                //     }
                // }
                eventer.emit('module.close');
                currentModule = false;
                
                // Sometimes slick is reinitialized and
                // not look good on page, goTo fix this
                // abruptely

                $timeout(function () {
                    //console.log("ciiiiiii")
                    var lastModuleViewed = service.getLastModuleViewed();
                    

                    function scrollTo(hash) {
                        location.hash = "#" + hash;
                        console.log("ci sono"+hash)
                    }


                    //console.log("lastModuleViewed",lastModuleViewed);
                    
                    if (lastModuleViewed == 0) {
                        //$location.hash('section2');
                        scrollTo("section1");
                    }else if(lastModuleViewed == 1){
                        //$location.hash('section2');
                        scrollTo("section2");
                    }else if(lastModuleViewed == 2){
                        //$location.hash('section3');
                        scrollTo("section2");
                    }else if(lastModuleViewed == 3){
                        //$location.hash('section3');
                        scrollTo("section3");
                    }else if(lastModuleViewed == 4){
                        //$location.hash('section4');
                        scrollTo("section3");
                    }else if(lastModuleViewed == 5){
                        //$location.hash('section4');
                        scrollTo("section4");
                    }else if(lastModuleViewed == 6){
                        //$location.hash('section5');
                        scrollTo("section4");
                    }else if(lastModuleViewed == 7){
                        //$location.hash('section5');
                        scrollTo("section5");
                    }else if(lastModuleViewed == 8){
                        //$location.hash('section6');
                        scrollTo("section5");
                    }else if(lastModuleViewed == 9){
                        //$location.hash('section6');
                        scrollTo("section6");
                    }else if(lastModuleViewed == 10){
                        //$location.hash('section7');
                        scrollTo("section6");
                    }else if(lastModuleViewed == 11){
                        //$location.hash('section7');
                        scrollTo("section7");
                    }else if(lastModuleViewed == 12){
                        //$location.hash('section8');
                        scrollTo("section7");
                    }else if(lastModuleViewed == 13){
                        //$location.hash('section8');
                        scrollTo("section8");
                    }else if(lastModuleViewed == 14){
                        //$location.hash('section8');
                        scrollTo("section8");
                    }

                   


                    
                    //$anchorScroll();


                   
                    //eventer.emit('module.closeScroll',lastModuleViewed);
                    loadedModule=false
                   

                    //console.log("app chiudo modulo")
                    
                    /*
                    var lastModuleViewed = service.getLastModuleViewed();
                    console.log(lastModuleViewed)
                    if (lastModuleViewed > 0 && lastModuleViewed <= 2) {
                        $location.hash('module1');
                    } else if (lastModuleViewed >= 3 && lastModuleViewed <= 5) {
                        $location.hash('module2');
                    } else if (lastModuleViewed >= 6 && lastModuleViewed <= 8) {
                        $location.hash('module3');
                    } else if (lastModuleViewed >= 9 && lastModuleViewed <= 11) {
                        $location.hash('module4');
                    } else if (lastModuleViewed == 12) {
                        $location.hash('module5');

                    }
                    
                    $anchorScroll();
                    */
                }, 1000)

                //$scope.hideBtnPopover();

               

                
                /*
                if($('slick').length) {
                    var slick = $('slick')[0].slick;
                    slick.goTo(slick.getCurrent());
                }
                */
            },

            isModuleClosed: function(){
                return loadedModule;
            },
            setFirstPageToView: function (i) {
                var page = this.pages()[i];
                var found = false;
                _.each(this.structure().all(), function (module, mKey) {
                    _.each(module.lessons().all(), function (lesson, lKey) {
                        _.each(lesson.pages().all(), function (p, pKey) {
                            if (page == p) {
                                currentModule = mKey;
                                module.setCurrentLesson(lKey);
                                lesson.setCurrentPage(pKey);
                                return;
                            }
                        });

                        if (found) return;
                    });

                    if (found) return;
                });

                return this;
            },
            /**
             * @description go to first page of lesson
             */
            goToFirstPageOfLesson: function () {
                this.module().lesson().pages().setCurrent(0);
                this.panels.resume.close();
                return this;
            },
            goToPageByIndex: function (i) {
                i = i - 1;
                // console.log(i);

                var found = false;
                var page = this.pages()[i];
                // console.log(page.getAttr('title'));
                _.each(this.structure().all(), function (module, mKey) {
                    _.each(module.lessons().all(), function (lesson, lKey) {
                        _.each(lesson.pages().all(), function (p, pKey) {
                            if (page == p) {
                                currentModule = mKey;
                                service.modules().setCurrent(currentModule);
                                module.setCurrentLesson(lKey);
                                lesson.setCurrentPage(pKey);
                                return;
                            }
                        });

                        if (found) return;
                    });

                    if (found) return;
                });

                this.panels.resume.close();
            },
            config: function () {
                return config;
            },
            panels: panels,
            logout: function () {
                panels.logout.open();
                eventer.emit('logout');
            },
            refresh: function () {
                eventer.emit('refresh');
                return false;
            },
            isAtLastPage: function () {
                return this.getCurrentPageNumber() == this.pages().length;
            },
            hasNextPage: function () {

                if (this.config().isNavLimitedOnLesson())
                    if (this.module().lesson().pages().isAtTheEnd()) return false;

                if (!this.module().lesson().pages().isAtTheEnd())
                    return true;

                if (!this.module().lessons().isAtTheEnd())
                    return true;

                if (!this.modules().isAtTheEnd())
                    return true;

                return false;
            },
            hasPrevPage: function () {
                if (this.config().isNavLimitedOnLesson()) {
                    if (this.module().lesson().pages().isAtTheStart()) return false;
                    return true;
                }

                if (!this.modules().isAtTheStart())
                    return true;

                if (!this.module().lessons().isAtTheStart())
                    return true;

                if (!this.module().lesson().pages().isAtTheStart())
                    return true;

                return false;
            },
            events: eventer,
            exitFullscreen: function () {
                if (document.fullscreenElement && document.exitFullscreen) {
                    document.exitFullscreen();
                }
                else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                }
                else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
                else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
                else if (document.msCancelFullScreen) {
                    document.msCancelFullScreen();
                }
            },
            getSlidesToShow: function () {
                var w = $(window).width();
                // return 1;

                switch (true) {
                    case (w <= 768):
                        return 4;
                        break;

                    case (w > 1200):
                        return 6;
                        break;

                    case (w <= 1200):
                        return 8;
                        break;
                }
            },
            /**
             * @description get score lesson
             * @return {number}
             */
            score() {
                let score = 0;
                this.modules().forEach((module) => score += module.score());
                // console.log("AppSCORE", score);
                return score;
            },

            /**
             * @description get score lesson (only lesson with exercises)
             * @return {number}
             */
            scoreEx() {
                let score = 0;
                this.modules().forEach((module, index) => {
                    if (index > 0 && index < 5) score += module.score()
                }
                );
                // console.log("AppSCORE", score);
                return score;
            },

            percScore() {
                return Math.round(this.score() * 100 / this.maxScore());
            },

            /**
             * @description get max score
             * @return {number}
             */
            maxScore() {
                let score = 0;
                this.modules().forEach(module => score += module.maxScore());
                return score;
            },
            /**
             * @description get max score (only lesson with exercises)
             * @return {number}
             */
            maxExScore() {
                let score = 0;
                this.modules().forEach((module, index) => {
                    if (index > 0 && index < 5) score += module.maxScore()
                });
                return score;
            },
            getLastViewedModuleID() {
                let id = 14;
                let mod = _.find(this.modules().all(), (module, index) => {
                    if (!module.isViewed()) { id = index; return true; };
                });
                return id;
            },
            getGaugeValue() {
                return Math.round(this.scoreEx() * 100 / this.maxExScore());
            },
            toggleAudio: function () {
                if (this.module().lesson().page().video()) {
                    var audioIsMuted = !this.module().lesson().page().video().player().muted()
                    this.module().lesson().page().video().player().muted(audioIsMuted);
                }
            },
            audioIsPlaying: function () {
                return this.module().lesson().page().video() ? (!this.module().lesson().page().video().player().muted()) : false;
            },
            isCurrentModule: function (module) {
                return (this.module() == module);
            },
            areAllModulesViewed: function () {
                var allViewed = true;
                this.modules().forEach((module) => {
                    if (!module.isViewed()) allViewed = false;
                });
                return allViewed;
            },
            goToPageByAbsIndex: function (pageIndex) {

                var _this = this;
                var moduleIndex = _.indexOf(_this.modules().all(), _this.module());
                var lessonIndex = _.indexOf(_this.module().lessons().all(), _this.lesson());

                if (
                    pageIndex > 0 &&
                    !_this.module().lessons().get(lessonIndex).pages().get(pageIndex - 1).isViewed()
                    && !config.isFree()
                ) {
                    return;
                }

                _this.modules().setCurrent(moduleIndex);
                _this.module().setCurrentLesson(lessonIndex);
                _this.module().lesson().setCurrentPage(pageIndex);

                eventer.emit('menu.change');

                return _this;
            },
            goWhereYouLeft: function () {
                console.log("this.isThereAUnitPending()",this.isThereAUnitPending())
                if (this.isThereAUnitPending()) {

                    currentModule = this.getLastViewedModuleID();
                    console.log("currentModule",currentModule)
                    if (!config.isFree() && (currentModule > 0 && !this.modules().get(currentModule - 1).isViewed()))
                        return false;

                    eventer.emit('module.open.pre');

                    this.modules().setCurrent(currentModule);
                    this.module().setCurrentLesson(0);

                    // if(!this.module().lesson().pages().isAtTheStart()) {
                    //     console.log("OPEN");
                    //     panels.resume.open();
                    // }

                    eventer.emit('module.open');
                }

                this.panels.resume.close();

                return this;

            },
            justCloseResumePanel: function () {
                // Reset bookmark for all units
                _.each(modules.all(), function (m) {
                    _.each(m.lessons().all(), function (l) {
                        l.setCurrentPage(0);
                    });
                });

                this.panels.resume.close();
            },
            isResumable: function () {
                var _return = true;

                this.isThereAUnitPending();

                if (isResumed) {
                    _return = false;
                    return _return;
                }

                if (this.areAllModulesViewed()) {
                    _return = false;
                    return _return;
                }

                if (
                    this.getLastViewedModuleID() == 0 &&
                    this.modules().get(0).lesson().pages().currentIndex() == 0
                ) {
                    _return = false;
                    return _return;
                }

                panels.resume.open();

                return _return;
            },
            isThereAUnitPending: function () {
                var _return = false;
                _.each(modules.all(), function (m) {
                    _.each(m.lessons().all(), function (l) {
                        var pagesCompletions = _.countBy(_.invoke(l.pages().all(), 'isViewed'));
                        console.log("pagesCompletions",pagesCompletions)
                        // console.log("IF", l.pages().isAtTheStart() && !! pagesCompletions.true && !! pagesCompletions.false);
                        // console.log(l.pages().isAtTheStart(), !! pagesCompletions.true, !! pagesCompletions.false);
                        /*
                        if (
                            // l.pages().isAtTheStart() && 
                            !!pagesCompletions.true && !!pagesCompletions.false) {
                            _return = true;
                            return _return;
                        }
                        */
                        if (
                            // l.pages().isAtTheStart() && 
                            pagesCompletions.false) {
                            _return = true;
                            return _return;
                        }
                        // console.log("---------------------------------------------");
                    });
                });
                // console.log("*** isThereAUnitPending?", _return);
                return _return;
            },
            hasFinalAudioBeenPlayed: function () {
                return audioEndedPlayed;
            },
            setFinalAudioAsPlayed: function () {
                audioEndedPlayed = true;
                return this;
            },
            isMobile: function () {
                return isMobile.any;
            },
            isPhone: function () {
                return isMobile.phone;
            }
        };

        var isResumed = false;

        panels.closer.on('open', function () {
            // $timeout(function() {
            if (service.module().lesson().page().video()) {
                service.module().lesson().page().video().pause();
            }
            // }, 500);
        });

        panels.resume.on('close', function () {
            isResumed = true;

            // $timeout(function() { 
            // if (service.module().lesson().page().video())
            //     service.module().lesson().page().video().play();
            // }, 500);
        });

        eventer.on('module.open', function () {
            isResumed = true;
            if (audioEndedInstance) {
                audioEndedInstance.stop();
            }
        });

        return service;
    }]);
