/**
 * params:
 *  - timerDuration <-- in ms, indica la durata del timer
 *  - timeElapsedMs <-- in ms, indica quanti millisecondi sono trascorsi dall'avvio del timer
 *  - timeStarted <-- true/false, cambiando lo stato dal controller contenitore della direttiva, parte o viene interrotto il timer
 *  - onTimeOverFn <-- la direttiva esegue questa funzione quando scade il timer (gli viene passata dall'esterno)
 *
 *  nel controller contenitore si può inviare l'evento $scope.$broadcast("RESTART_TIMER"); per riavviare il timer
 */
(function(){

    'use strict';

    var Timer = function($scope, $interval) {

        return {
            restrict: "E",
            scope:{
                timerDuration: '=',
                timeElapsedMs: '=',
                timeStarted: '=',
                onTimeOverFn: '='
            },
            templateUrl: "../partials/directives/timer.html",
            controller: ['$scope','$interval', function TimerController($scope, $interval){


            }],
            link: function($scope, $element, $attr, ctrl, transclude){
                var startTime = null;
                $scope.timer = $scope.timerDuration || 3000;
                var timerInterval;
                $scope.animationBlinkTimer = false;
                const START_BLINKING_TIMER = 6;
                const OVER_TIME_MESSAGE_AUDIO = new buzz.sound("audio/sfx/wrong_sound",{ formats: [ "mp3"], preload: true});




                var startTimer = function(){
                    $interval.cancel(timerInterval);
                    $scope.timeElapsedMs = 0;
                    startTime = (new Date()).getTime();
                    /*var overFn = function(){
                        TweenMax.to($(this), 0.3, {scaleX: 1.1,scaleY: 1.1});
                    }
                    var outFn = function(){
                        TweenMax.to($(this), 0.3, {scaleX: 1,scaleY: 1});
                    }

                    $(".choice").hover(overFn, outFn)*/

                    timerInterval = $interval(function(){
                        $scope.timeElapsedMs = (new Date()).getTime() - startTime;
                        if ($scope.timeElapsedMs >= $scope.timer){
                            stopTimer();
                            OVER_TIME_MESSAGE_AUDIO.play();
                            $scope.onTimeOverFn();
                        }else{
                            var seconds = Math.trunc(($scope.timer - $scope.timeElapsedMs)/1000);
                            if (seconds < START_BLINKING_TIMER){
                                $scope.animationBlinkTimer = true;
                            }
                            if (seconds < 10){
                                seconds = "0"+seconds;
                            }
                            var millis = Math.trunc((($scope.timer - $scope.timeElapsedMs)%1000)/10);
                            if (millis < 10){
                                millis = "0"+millis;
                            }
                            $scope.countdown = seconds + ":" + millis;

                        }
                    }, 10);
                }
                $scope.$on('RESTART_TIMER', startTimer);

                var stopTimer = function(){
                    $scope.timeStarted = false;
                    $interval.cancel(timerInterval);
                }

                $scope.$watch(
                    function(scope) { return scope.timeStarted },
                    function( newValue, oldValue ) {
                        // If enabled, listen for mouse events.
                        if ( newValue ) {
                            startTimer();
                        } else if ( oldValue ) {
                            stopTimer();
                        }
                    }
                );

                $scope.$on('$destroy', function() {
                    stopTimer();
                });
            }
        }
    };

    Timer.$inject = ['$compile','$interval'];

    app.directive('timer', Timer);

})();