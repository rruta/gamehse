(function(){

    'use strict';

    var AutoresizedContainerForVideo = function($compile, $timeout) {

        return {
            restrict: "E",
            scope: {
                "onClick": "=",
                "visibleLayoutHeight": "="
            },
            templateUrl: "../partials/directives/autoresized-container-for-video.template.html",
            transclude: true,
            controller: ['$scope', function AutoresizedContainerForVideoController($scope){
                const IMG_WIDTH = 1920;
                const IMG_HEIGHT = 1080;
                var resize = function(){

                    $scope.parentContainerWidth = window.innerWidth;
                    $scope.parentContainerHeight = window.innerHeight;

                    if ($scope.visibleLayoutHeight){
                        $scope.parentContainerHeight -= $scope.visibleLayoutHeight;
                    }

                    /* spazi neri sopra/sotto video  */
                    $scope.containerWidth = window.innerWidth;
                    $scope.containerHeight = window.innerWidth*IMG_HEIGHT/IMG_WIDTH;

                    /* spazi neri sinistra/destra video  */
                    if ($scope.containerHeight > window.innerHeight){
                        $scope.containerHeight = window.innerHeight;
                        $scope.containerWidth = window.innerHeight*IMG_WIDTH/IMG_HEIGHT;
                    }
                }

                window.onresize = function(){
                    resize();
                    $timeout(function(){
                        $scope.$apply();
                    },0)
                }
                resize();
                $timeout(function(){
                    $scope.$apply();
                },0)


                $scope.$on("$destroy", function() {
                    window.onresize = null;
                });

                $scope.onAreaClicked = function(){
                    $scope.onClick();
                }
            }]/*,
            link: function($scope, $element, $attr, ctrl, transclude){
                transclude(function (clone) {
                    if (clone.length) {
                        $element.replaceWith(clone);
                    }
                    else {
                        $element.remove();
                    }
                });
            }*/
        }
    };

    AutoresizedContainerForVideo.$inject = ['$compile','$timeout'];

    app.directive('autoresizedContainerForVideo', AutoresizedContainerForVideo);

})();