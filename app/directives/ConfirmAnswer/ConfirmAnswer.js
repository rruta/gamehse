/**
 * params:
 *  - timerDuration <-- in ms, indica la durata del timer
 *  - timeElapsedMs <-- in ms, indica quanti millisecondi sono trascorsi dall'avvio del timer
 *  - timeStarted <-- true/false, cambiando lo stato dal controller contenitore della direttiva, parte o viene interrotto il timer
 *  - onTimeOverFn <-- la direttiva esegue questa funzione quando scade il timer (gli viene passata dall'esterno)
 *
 *  nel controller contenitore si può inviare l'evento $scope.$broadcast("RESTART_TIMER"); per riavviare il timer
 */
(function(){

    'use strict';

    var ConfirmAnswer = function($scope) {

        return {
            restrict: "E",
            scope:{
                onConfirm: '&',
                onCancel: '&'
            },
            templateUrl: "../partials/directives/confirm-answer.template.html",
            link: function($scope, $element, $attr, ctrl, transclude){

            }
        }
    };

    ConfirmAnswer.$inject = ['$compile'];

    app.directive('confirmAnswer', ConfirmAnswer);

})();